# Meia Gaspea Assets (Originais)

## Description

Um simples repositório para guardar os assets

A simple repository to store assets

## Links

### Social Networks :)

-   [Youtube](https://www.youtube.com/channel/UCUqmAAgOoVVMpxykzPNCSUw);
-   [Blog](https://meiagaspeaonline.blogspot.com/);
-   [Instagram](https://www.instagram.com/meiagaspea/);
-   [More](https://meiagaspeaonline.blogspot.com/p/redes-sociais.html);

### Tools

#### Audio

-   [SFXT](https://sfxr.me/);

#### Sprites/Textures/etc...

-   Microsoft Paint :)
-   [Piskel](https://www.piskelapp.com/);
-   [Krita](https://krita.org/);
-   [Gimp](https://www.gimp.org/);

#### 3D

-   [Blender](https://www.blender.org/);
-   [SketchUp](https://www.sketchup.com/);
-   [Three Editor](https://threejs.org/editor/);
